<div  align="center">
<img  src="https://raw.githubusercontent.com/netbox-community/netbox/develop/docs/netbox_logo.svg"  height="100"  alt="NetBox logo"  />  
 <img  src=https://raw.githubusercontent.com/kubernetes/kubernetes/master/logo/logo.svg  height="100"  alt="NetBox logo"  />
</div>

# Netbox kubernetes deployment using ArgoCD

This repo contains a demo for a full ArgoCD deployment of netbox for kubernetes.

This repo references opensource projects like netbox, bitnami's helm charts and ArgoCD. For more information on these projects, please go check them out (more links in the [References](https://gitlab.com/rodrigo.ferreiraAX/atc-netbox/-/tree/staging#references) section):

> "NetBox is an infrastructure resource modeling (IRM) tool designed to empower network automation."
> 
> -- <cite>[GitHub - netbox-community/netbox](https://github.com/netbox-community/netbox)</cite>

> "Kubernetes, also known as K8s, is an open source system for managing [containerized applications](https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/) across multiple hosts."
> 
> -- <cite>[GitHub - kubernetes/kubernetes: Production-Grade Container Scheduling and Management](https://github.com/kubernetes/kubernetes)</cite>
  

> "Argo CD is a declarative, GitOps continuous delivery tool for Kubernetes."
> 
> -- <cite>[GitHub - argoproj/argo-cd: Declarative continuous deployment for Kubernetes.](https://github.com/argoproj/argo-cd)</cite>

This deployment is not production ready. It does not make use of HA mechanisms. This repo should only be used for demo purposes. For some production-ready recommendations, please check [Production-ready deployment](https://gitlab.com/rodrigo.ferreiraAX/atc-netbox#production-ready-deployment-advanced) section.

Kustomize is used in this repo to manage the .yaml kubernetes manifest files.

## Architecture

A single netbox instance stores its data on a postgres database and makes use of one redis server to store data and another to cache sessions.

The demo in this repo does not contain an ingress controller. To access the app either deploy a kubernetes ingress controller and an ingress object or port-forward the service to access locally.

## Folder tree

Here follows the used folder tree and where ArgoCD should point when you deploy the application in the next step.

Notice that the base files are shared across overlays. Overlays patch configurations on top of the base folder manifests.

```
atc-netbox
│   README.md    
│
└───argocd-config  <-- ArgoCD application objects that will deploy the netbox environments
│   └───base 
│   │   |   kustomization.yaml 
│   │   |   netbox-netbox.yaml  <-- points to "netbox-deploy/overlay/..." on this repo
│   │   |   netbox-postgesql.yaml  <-- points to bitnami's postgresql helm chart
│   │   |   netbox-redis.yaml  <-- points to bitnami's redis helm chart
│   │   |   netbox-rediscache.yaml  <-- points to bitnami's redis helm chart
│   │
│   └───overlay
|       └───production <-- !!point your ArgoCD config PRODUCTION application to this folder!!
|       |   |   kustomization.yaml
|       |   |   ...
|       |
|       └───staging  <-- !!point your ArgoCD config STAGING application to this folder!!
|           |   kustomization.yaml
|           |   ...
│   
└───netbox-deploy  <-- Netbox kubernetes manifest files
    └───base
    │   |   kustomization.yaml 
    │   |   netbox-pd.yaml
    │   |   netbox-pvc.yaml
    │   |   netbox-secret.yaml
    │   |   netbox-svc.yaml
    │   
    └───overlay
        └───production
        |   |   kustomization.yaml
        |   |   ...
        |
        └───staging
            |   kustomization.yaml
            |   ...
```

## Deploy

This deployment requires an ArcgCD instance.

The application is split into two environments:
 - staging (staging branch)
 - production (main branch)

You can deploy both environments with a single branch, but it's recommended pointing each branch to its own environment.

### Prerequisits

 - Install "kubectl" CLI
 - Bring your own kubernetes cluster to target the deployment (use for ex Kind, Minikube or docker desktop kubernetes instance)
 - Bring your own instance of ArgoCD
 - Fork this repo and add the forked repo to ArgoCD
 - In your forked repo, edit the "repoURL" in the"argocd-config/base/netbox-netbox.yaml" file to reflect your repo.
 - Add the Bitnami helm repo to ArgoCD
 - Create a project in ArgoCD with permissions on the "argocd" namespace (for the ArgoCD application object creation)
 - Create a project in ArgoCD with permissions on the "staging" namespace (for the staging instance of the app)
 - Create a project in ArgoCD with permissions on the "production" namespace (for the production instance of the app)

### App deployment

To deploy netbox, create an application in ArgoCD and point the source repo to your forked repo, branch *DESIRED-BRANCH*, path "/argocd-config/overlay/*DESIRED-ENVIRONMENT*", where *DESIRED-BRANCH* can be "staging" or "main" and *DESIRED-ENVIRONMENT* can be "staging" or "production" respectivelly, and target your kubernetes cluster where the ArgoCD is instaled (namespace "argocd").

Alternatively you can apply the ArgoCD application objects directly by running the following command:

```console
kubectl create ns staging
kubectl -n argocd apply -k argocd-config/overlay/stagging
kubectl create ns production
kubectl -n argocd apply -k argocd-config/overlay/production
```

After that, the only thing left to do is sync the ArgoCD applications. 
 
### Login to netbox

If you want to access the staging application on *localhost:8081*, run the following "kubectl" command:

```console
kubectl -n staging port-forward svc/netbox-staging 8081:8081
```

In the browser, open http://localhost:8081 and login with the following credentials:
 - user: admin
 - password: Locallab1234

The admin password can be changed in the netbox secret object though the "netbox-deploy/base/netbox-secret.yaml" manifest file.

### Production-ready deployment (advanced)

Feel free to fork this project and change it for a more production-ready deployment.
Here follows some suggestions:
 - Use a multi master multi worker node kubernetes cluster to deploy the application
 - deploy postgres as an external service or change its chart to bitnami's postgresql-ha chart for high-availability [GitHub - bitnami/charts/postgesql-ha](https://github.com/bitnami/charts/tree/master/bitnami/postgresql-ha)
 - use bitnami's redis-cluster chart for redis and rediscache for this project. Notice that the redis chart used in this project can accommodate multiple databases (which is not happening in this example :) ) but redis-cluster can only accommodate one database per cluster [GitHub - bitnami/charts/redis-cluster](https://github.com/bitnami/charts/tree/master/bitnami/redis-cluster)
 - use at lease 3 replicas for netbox deployment. Edit it in the production overlay! make use of kustomization if you have a multi environment deployment.
 - edit the netbox pod resources acording to the expected load. Edit the production overlay! make use of kustomization if you have a multi environment deployment.
 - the netbox-media-files persistent volume has to be readwritemany for it to be shared between netbox replicas. Make sure your container storage interface supports it.
 - use pod antiaffinity for the netbox deployment to ensure pod distribution across kubernetes hosts. [Kubernetes - Inter-pod affinity and anti-affinity](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#inter-pod-affinity-and-anti-affinity)
 - deploy an ingress controller to allow loadbalanced L7 ingress. Try projectcontour or nginx [Kubernetes- Ingress Controllers](https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/)
 - enable auto-sync for the ArgoCD applications

## References

Netbox GitHub:
[GitHub - netbox-community/netbox](https://github.com/netbox-community/netbox)

Bitnami helm charts GitHub:
[GitHub - bitnami/charts](https://github.com/bitnami/charts)

ArgoCD getting started:
[Getting Started - Argo CD - Declarative GitOps CD for Kubernetes](https://argo-cd.readthedocs.io/en/stable/getting_started/)

Kustomize:
[Kustomize - Kubernetes native configuration management](https://kustomize.io/)

---
**Lab by:**

Rodrigo Ferreira

Systems Eng

[rodrigo.ferreira@axians.com](mailto:rodrigo.ferreira@axians.com)
<div><img src="https://cdn.directions.pt/uploads/2020/07/kisspng-vinci-sa-business-information-and-communications-t-portugal-5ad1d33be4e178.7292876315237005399375-1.png"  width="200"  alt="NetBox logo"  /></div>